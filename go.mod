module gitlab.com/adred/todo

go 1.16

require (
	github.com/gocql/gocql v0.0.0-20211222173705-d73e6b1002a7
	github.com/joho/godotenv v1.4.0
	github.com/labstack/echo/v4 v4.6.3
)
