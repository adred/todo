# Base image for building the project
FROM golang:alpine AS build

# Update the repository and install git
RUN apk --no-cache add gcc g++ make git

ARG USER
ARG KEY

RUN git config --global url."https://${USER}:${KEY}@gitlab.com/".insteadOf "https://gitlab.com/"
RUN export GOPRIVATE=*gitlab*

# Switches to /tmp/app as the working directory
WORKDIR /tmp/app
COPY . .

# Builds the current project to a binary called web-app
RUN GOOS=linux go build -ldflags="-s -w" -o ./bin/todo ./cmd/todo/main.go

# Now that the project has been successfully built, we will use
# alpine image to run the server
FROM alpine:3.13

# Add CA certificates to the image
RUN apk --no-cache add ca-certificates

# Switch working directory to /usr/bin
WORKDIR /usr/bin

# Copies the binary file from the BUILD container to /usr/bin
COPY --from=build /tmp/app/bin /usr/bin

# Exposes port 9000 from the container
EXPOSE 9000

ENTRYPOINT /usr/bin/todo