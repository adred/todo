### Requirements

1. Go v1.16
2. Latest CassandraDB installation using default port

### Configuration

1. Edit `.env` file and change `DB_HOST` to your CassandraDB IP Address
2. In Cassandra, create a `keyspace` called `todo` and table inside it called `tasks`. I'm assuming you're using the the default `user` and `password` so I hardcoded it in the code.

### Run

1. cd to `todo`
2. `go mod tidy`
3. cd to `todo/cmd/todo`
4. `go run main.go`. If it succeeded connecting to the DB, it should run on port `8080`
5. You can try the endpoints via Postman on `http://localhost:8080/api/tasks`

### Endpoints

1. POST `api/tasks`. Creates task. JSON payload is

```
{
	"id": "1",
	"name": "Task Name",
	"description": "Task Description",
	"done": 0
}
```

2. GET `api/tasks`. Fetches all tasks.
3. GET `api/tasks/:id`. Fetches one task by ID.
4. PUT `api/tasks/:id`. Updates a task. Same payload as Create.
5. DELETE `api/tasks/:id`. Deletes a task.
