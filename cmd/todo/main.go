package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gocql/gocql"
	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/adred/todo/internal/handler"
	"gitlab.com/adred/todo/internal/repository"
	"gitlab.com/adred/todo/internal/service"
)

func init() {
	// Load env vars
	if err := godotenv.Load("../../.env"); err != nil {
		log.Println("no env gotten")
	}
}

func main() {
	// DB vars
	host := os.Getenv("DB_HOST")
	// API
	port := os.Getenv("PORT")

	var err error

	cluster := gocql.NewCluster(host)
	cluster.Keyspace = "todo"
	cluster.Consistency = gocql.Quorum
	cluster.Authenticator = gocql.PasswordAuthenticator{Username: "cassandra", Password: "cassandra"}

	session, err := cluster.CreateSession()
	if err != nil {
		panic(err)
	}

	tr, err := repository.NewTaskRepository(session)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	ts := service.NewTaskService(tr)
	th := handler.NewTaskHandler(ts)

	r := echo.New()
	// Secure
	r.Use(middleware.Secure())
	// CORS
	r.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"http://localhost:3000"},
		AllowHeaders:     []string{echo.HeaderContentType},
		AllowCredentials: true,
	}))

	// Routes
	r.POST("api/tasks", th.CreateTask)
	r.GET("api/tasks", th.GetAllTasks)
	r.GET("api/tasks/:id", th.GetTask)
	r.PUT("api/tasks/:id", th.UpdateTask)
	r.DELETE("api/tasks/:id", th.DeleteTask)

	// Start application
	rPort := port
	if rPort == "" {
		rPort = "8888" //localhost
	}
	log.Fatal(r.Start(":" + rPort))
}
