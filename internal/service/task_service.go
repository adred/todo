package service

import (
	"gitlab.com/adred/todo/internal/domain"
	"gitlab.com/adred/todo/internal/repository"
)

type TaskServiceInterface interface {
	CreateTask(*domain.TaskEntity) (*domain.TaskEntity, error)
	GetTask(id string) (*domain.TaskEntity, error)
	GetAllTasks() ([]*domain.TaskEntity, error)
	UpdateTask(task *domain.TaskEntity) (*domain.TaskEntity, error)
	DeleteTask(id string) error
}

type TaskService struct {
	taskRepo *repository.TaskRepository
}

func NewTaskService(tr *repository.TaskRepository) *TaskService {
	return &TaskService{taskRepo: tr}
}

// TaskService implements the domain.UserInterface interface
var _ TaskServiceInterface = &TaskService{}

func (ts *TaskService) CreateTask(task *domain.TaskEntity) (*domain.TaskEntity, error) {
	task, err := ts.taskRepo.CreateTask(task)
	if err != nil {
		return nil, err
	}

	return task, nil
}

func (ts *TaskService) GetTask(id string) (*domain.TaskEntity, error) {
	task, err := ts.taskRepo.GetTask(id)
	if err != nil {
		return nil, err
	}
	return task, nil
}

func (ts *TaskService) GetAllTasks() ([]*domain.TaskEntity, error) {
	tasks, err := ts.taskRepo.GetAllTasks()
	if err != nil {
		return nil, err
	}
	return tasks, nil
}

func (ts *TaskService) UpdateTask(task *domain.TaskEntity) (*domain.TaskEntity, error) {
	task, err := ts.taskRepo.UpdateTask(task)
	if err != nil {
		return nil, err
	}

	return task, nil
}

func (ts *TaskService) DeleteTask(id string) error {
	err := ts.taskRepo.DeleteTask(id)
	if err != nil {
		return err
	}

	return nil
}
