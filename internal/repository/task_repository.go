package repository

import (
	"github.com/gocql/gocql"

	"gitlab.com/adred/todo/internal/domain"
)

var (
	createTaskQuery  = "INSERT INTO tasks ( id, name, description, done) VALUES(?, ?, ?, ?)"
	getTaskQuery     = "SELECT id, name, description, done FROM tasks WHERE id=? LIMIT 1"
	getAllTasksQuery = "SELECT * FROM tasks"
	updateTaskQuery  = "UPDATE tasks SET name = ?, description = ?, done = ? WHERE id = ?"
	deleteTaskQuery  = "DELETE FROM tasks WHERE id = ?"
)

type TaskRepositoryInterface interface {
	CreateTask(*domain.TaskEntity) (*domain.TaskEntity, error)
	GetTask(id string) (*domain.TaskEntity, error)
	GetAllTasks() ([]*domain.TaskEntity, error)
	UpdateTask(task *domain.TaskEntity) (*domain.TaskEntity, error)
	DeleteTask(id string) error
}
type TaskRepository struct {
	session *gocql.Session
}

func NewTaskRepository(session *gocql.Session) (*TaskRepository, error) {
	return &TaskRepository{
		session,
	}, nil
}

var _ TaskRepositoryInterface = &TaskRepository{}

func (tr *TaskRepository) CreateTask(task *domain.TaskEntity) (*domain.TaskEntity, error) {
	if err := tr.session.Query(createTaskQuery,
		task.ID, task.Name, task.Description, task.Done).Exec(); err != nil {
		return &domain.TaskEntity{}, err
	}
	return task, nil
}

func (tr *TaskRepository) GetTask(id string) (*domain.TaskEntity, error) {
	var rID string
	var name string
	var description string
	var done int

	err := tr.session.Query(getTaskQuery,
		id).Scan(&rID, &name, &description, &done)

	if err != nil {
		return &domain.TaskEntity{}, err
	}

	return &domain.TaskEntity{
		ID:          rID,
		Name:        name,
		Description: description,
		Done:        done,
	}, nil
}

func (tr *TaskRepository) GetAllTasks() ([]*domain.TaskEntity, error) {
	var tasks []*domain.TaskEntity
	t := map[string]interface{}{}

	iter := tr.session.Query(getAllTasksQuery).Iter()
	for iter.MapScan(t) {
		tasks = append(tasks, &domain.TaskEntity{
			ID:          t["id"].(string),
			Name:        t["name"].(string),
			Description: t["description"].(string),
			Done:        t["done"].(int),
		})
		t = map[string]interface{}{}
	}

	return tasks, nil
}

func (tr *TaskRepository) UpdateTask(task *domain.TaskEntity) (*domain.TaskEntity, error) {
	if err := tr.session.Query(updateTaskQuery, task.Name, task.Description, task.Done, task.ID).Exec(); err != nil {
		return &domain.TaskEntity{}, err
	}

	return task, nil
}

func (tr *TaskRepository) DeleteTask(id string) error {
	if err := tr.session.Query(deleteTaskQuery, id).Exec(); err != nil {
		return err
	}

	return nil
}
