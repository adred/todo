package handler

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/adred/todo/internal/domain"
	"gitlab.com/adred/todo/internal/service"
)

// TaskHandler struct defines the dependencies that will be used
type TaskHandler struct {
	ts *service.TaskService
}

// NewTaskHandler constructor
func NewTaskHandler(ts *service.TaskService) *TaskHandler {
	return &TaskHandler{
		ts,
	}
}

func (th *TaskHandler) CreateTask(c echo.Context) error {
	var task domain.TaskEntity
	if err := c.Bind(&task); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, "invalid json")
	}

	t, err := th.ts.CreateTask(&task)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Something went wrong")
	}

	return c.JSON(http.StatusCreated, t)
}

func (th *TaskHandler) GetTask(c echo.Context) error {
	taskID := c.Param("id")

	task, err := th.ts.GetTask(taskID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "Something went wrong")
	}
	return c.JSON(http.StatusOK, task)
}

func (th *TaskHandler) GetAllTasks(c echo.Context) error {
	var tasks []*domain.TaskEntity
	tasks, err := th.ts.GetAllTasks()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "Something went wrong")
	}
	return c.JSON(http.StatusOK, tasks)
}

func (th *TaskHandler) UpdateTask(c echo.Context) error {
	var task domain.TaskEntity
	if err := c.Bind(&task); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, "invalid json")
	}

	t, err := th.ts.UpdateTask(&task)
	if err != nil {
		fmt.Println(err)
		return c.JSON(http.StatusInternalServerError, "Something went wrong")
	}

	return c.JSON(http.StatusCreated, t)
}

func (th *TaskHandler) DeleteTask(c echo.Context) error {
	taskID := c.Param("id")

	err := th.ts.DeleteTask(taskID)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, "Something went wrong")
	}
	return c.JSON(http.StatusOK, "Success")
}
