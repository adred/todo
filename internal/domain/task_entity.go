package domain

type TaskEntity struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Done        int    `json:"done"`
}
