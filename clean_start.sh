#!/bin/bash

# Delete container and image
sudo podman container rm todo -f
sudo podman image rm todo-image -f

# Build image and container
sudo podman build --build-arg USER=adred --build-arg KEY=glpat--hQ4PBxx48SimcKawAMy -t todo-image .
sudo podman run -d --name todo -v /home/adred/Dev/DecodeTech/todo:/usr/app:z --env-file .env --pod todo-pod todo-image



